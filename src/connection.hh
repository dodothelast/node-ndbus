#ifndef __CONNECTION_HH__
#define __CONNECTION_HH__

#include "common.hh"
#include "message.hh"

namespace ndbus {
  class Connection: public ObjectWrap {
    friend class Interface;
    friend class Member;
  public:
    DBusConnection *dbus_conn;
    
    string id();
  protected:
    uv_poll_t uv_poll;
    
    Connection(DBusConnection *conn);
    ~Connection();

    void poll_start();
    void poll_stop();
  public:
    static void Init(Handle<Object> target);
    
    static Handle<Value> Construct(const Arguments& args);
    static Handle<Value> GetId(const Arguments& args);

    O2S("connection [%p]", this);
  };
}

#endif//__CONNECTION_HH__
