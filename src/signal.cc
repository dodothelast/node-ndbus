#include "signal.hh"

namespace ndbus {
  Signal::Signal(Interface& interface_, string name_, string arg_)
    : Member(interface_, name_), arg(arg_) {
    LOG("  * %s", O2P(*this));
  }
  
  Signal::~Signal(){
    LOG("  ~ %s", O2P(*this));
  }
  
  string
  Signal::rule(){
    return "type='signal'" + (interface.service.length() ? ",sender='" + interface.service + "'" : "") + Member::rule();
  }
  
  bool
  Signal::dispatch(DBusMessage *dbus_mesg){
    if(Member::dispatch(dbus_mesg) &&
       dbus_message_is_signal(dbus_mesg, interface.name.c_str(), name.c_str())){
      LOG("  @ %s", O2P(*this));
      
      Event mesg(*this, dbus_mesg);
      
      HandleScope scope;

      Local<Context> context = Context::GetCurrent();
      
      Handle<Value> args[1] = {
        mesg.decode()
      };
      
      TryCatch try_catch;

      handler->Call(context->Global(), 1, args);
      
      HANDLE_CAUGHT(try_catch);
      
      scope.Close(Undefined());
      
      return true;
    }
    return false;
  }
  
  void
  Signal::Init(Handle<Object> target){
    Local<FunctionTemplate> tpl = FunctionTemplate::New(Construct);
    
    tpl->SetClassName(String::NewSymbol("DBusSignal"));
    tpl->InstanceTemplate()->SetInternalFieldCount(1);
    
    // Prototype
    SetPrototypeMethod(tpl, "emit", DoEmit);
    SetPrototypeMethod(tpl, "reg", OnEmit);
    
    target->Set(String::NewSymbol("Signal"), tpl->GetFunction());
  }
  
  /* new Signal(interface, "SignalName", "arg1_sig arg2_sig... argN_sig"); */
  Handle<Value>
  Signal::Construct(const Arguments& args){
    HandleScope scope;
    
    Interface *iface = ObjectWrap::Unwrap<Interface>(args[0]->ToObject());
    
    String::Utf8Value name(args[1]->ToString());
    String::Utf8Value arg(args[2]->ToString());
    
    Signal *signal = new Signal
      (*iface, *name, *arg);
    
    signal->Wrap(args.This());
    
    return scope.Close(args.This());
  }
  
  Emit::Emit(Signal &signal_)
    : Outgoing(), signal(signal_) {
    LOG("  * %s", O2P(*this));
    dbus_sign = signal.arg.c_str();
    dbus_mesg = dbus_message_new_signal
      (signal.interface.path.c_str(),
       signal.interface.name.c_str(), signal.name.c_str());
  }
  
  Emit::~Emit(){
    LOG("  ~ %s", O2P(*this));
  }
  
  Handle<Value>
  Emit::emit(){
    Connection& connection(signal.interface.connection);
    
    LOG("    do %s", O2P(*this));
    
    dbus_uint32_t serial = 0;
    if(!dbus_connection_send
       (connection.dbus_conn, dbus_mesg, &serial)){
      RET_ERROR(Error, "error signal emit: Out of Memory");
    }
    
    // blocking sending
    dbus_connection_flush(connection.dbus_conn);
    
    return Undefined();
  }
  
  /* Signal.emit(args); */
  Handle<Value> Signal::DoEmit(const Arguments& args){
    HandleScope scope;
    
    Signal *signal = ObjectWrap::Unwrap<Signal>(args.This());

    if(!signal){
      THROW_SCOPE(Error, "Signal object corrupted.");
    }
    
    Emit emit(*signal);
    
    unsigned count = args.Length();
    
    TryCatch try_catch;
    
    for(unsigned i = 0; i < count; i++){
      if(args[i]->IsArray()){
        emit.encode(Handle<Array>::Cast(args[i]));
      }
    }
    
    HANDLE_CAUGHT(try_catch)else{
      return scope.Close(emit.emit());
    }
    
    return scope.Close(Undefined());
  }
  
  /* Signal.reg([callback]); */
  Handle<Value> Signal::OnEmit(const Arguments& args){
    HandleScope scope;
    
    Signal *signal = ObjectWrap::Unwrap<Signal>(args.This());
    
    if(!signal){
      THROW_SCOPE(Error, "Signal object corrupted.");
    }
    
    if(args.Length() > 0){
      if(args[0]->IsFunction()){
        signal->attach(Handle<Function>::Cast(args[0]));
        return scope.Close(Boolean::New(true));
      }else{
        signal->detach();
        return scope.Close(Boolean::New(false));
      }
    }
    
    return scope.Close(signal->handler);
  }

  Event::Event(Signal& signal_, DBusMessage *mesg)
    : Incoming(mesg), signal(signal_) {
    dbus_sign = signal.arg.c_str();
  }
}
