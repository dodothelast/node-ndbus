#include "method.hh"

namespace ndbus {
  Method::Method(Interface& interface_, string name_, string arg_, string res_)
    : Member(interface_, name_), arg(arg_), res(res_) {
    LOG("  * %s", O2P(*this));
  }

  Method::~Method(){
    LOG("  ~ %s", O2P(*this));
  }
  
  string
  Method::rule(){
    return "type='method_call'" + (interface.service.length() ? ",destination='" + interface.service + "'" : "") + Member::rule();
  }
  
  bool
  Method::dispatch(DBusMessage *dbus_mesg){
    if(Member::dispatch(dbus_mesg) &&
       dbus_message_has_destination(dbus_mesg, interface.service.c_str()) &&
       dbus_message_is_method_call(dbus_mesg, interface.name.c_str(), name.c_str())){
      LOG("  @ %s", O2P(*this));
      
      Args mesg(*this, dbus_mesg);
      
      HandleScope scope;
      
      Local<Context> context = Context::GetCurrent();

      Result *res = new Result(*this, dbus_mesg);
      
      Local<Object> data = OnResData->NewInstance();
      data->SetPointerInInternalField(0, res);
      Local<FunctionTemplate> onres = FunctionTemplate::New(OnRes, data);
      
      TryCatch try_catch;
      
      Handle<Value> args[2] = {
        mesg.decode(),
        /* Asynchronous return */
        onres->GetFunction()
      };
      
      HANDLE_CAUGHT(try_catch)else{
        Handle <Value> ret = handler->Call(context->Global(), 2, args);
        
        HANDLE_CAUGHT(try_catch)else{
          if(ret->IsArray()){
            /* Synchronous return */
            res->encode(Handle<Array>::Cast(ret));
            
            HANDLE_CAUGHT(try_catch)else{
              res->send();
            }
            
            delete res;
          }
          
          scope.Close(Undefined());
        }
      }
      
      return true;
    }
    return false;
  }

  Handle<Value> Method::OnRes(const Arguments& args){
    HandleScope scope;
    
    Result *res = ObjectWrap::Unwrap<Result>(args.Data()->ToObject());
    
    if(!res){
      THROW_SCOPE(Error, "Result object corrupted.");
    }
    
    TryCatch try_catch;
    
    if(args.Length() > 0 && args[0]->IsArray()){
      res->encode(Handle<Array>::Cast(args[0]));
    }
    
    HANDLE_CAUGHT(try_catch)else{
      res->send();
    }
    
    delete res;
    
    return scope.Close(Undefined());
  }

  Args::Args(Method& method_, DBusMessage *mesg)
    : Incoming(mesg), method(method_) {
    LOG("  * %s", O2P(*this));
    dbus_sign = method.arg.c_str();
  }
  Args::~Args(){
    LOG("  ~ %s", O2P(*this));
  }

  Result::Result(Method& method_, DBusMessage *call)
    : Outgoing(), method(method_) {
    LOG("  * %s", O2P(*this));
    dbus_sign = method.res.c_str();
    dbus_mesg = dbus_message_new_method_return(call);
  }
  Result::~Result(){
    LOG("  ~ %s", O2P(*this));
  }

  Handle<Value>
  Result::send(){
    Connection& connection(method.interface.connection);
    
    LOG("    send %s", O2P(*this));

    dbus_uint32_t serial = 0;
    if(!dbus_connection_send
       (connection.dbus_conn, dbus_mesg, &serial)){
      RET_ERROR(Error, "error result send: Out of Memory");
    }
    
    // blocking sending
    dbus_connection_flush(connection.dbus_conn);
    
    return Undefined();
  }
  
  Persistent<ObjectTemplate>
  Method::OnResData;
  
  void
  Method::Init(Handle<Object> target){
    Local<FunctionTemplate> tpl = FunctionTemplate::New(Construct);
    
    tpl->SetClassName(String::NewSymbol("DBusMethod"));
    tpl->InstanceTemplate()->SetInternalFieldCount(1);
    
    // Prototype
    SetPrototypeMethod(tpl, "call", DoCall);
    SetPrototypeMethod(tpl, "reg", OnCall);
    
    OnResData = Persistent<ObjectTemplate>::New(ObjectTemplate::New());
    OnResData->SetInternalFieldCount(1);
    
    target->Set(String::NewSymbol("Method"), tpl->GetFunction());
  }
  
  /* new Method(interface, "MethodName", "arg1_sig arg2_sig... argN_sig", "res1_sig res2_sig... resNsig"); */
  Handle<Value>
  Method::Construct(const Arguments& args){
    HandleScope scope;
    
    Interface *iface = ObjectWrap::Unwrap<Interface>(args[0]->ToObject());
    
    String::Utf8Value name(args[1]->ToString());
    String::Utf8Value arg(args[2]->ToString());
    String::Utf8Value res(args[3]->ToString());
    
    Method *method = new Method
      (*iface, *name, *arg, *res);
    
    method->Wrap(args.This());
    
    return scope.Close(args.This());
  }

  Return::Return(Method& method_, DBusPendingCall *dbus_pend)
    : Incoming(), method(method_) {
    LOG("  * %s", O2P(*this));
    dbus_sign = method.res.c_str();
    dbus_mesg = dbus_pending_call_steal_reply(dbus_pend);
    dbus_pending_call_unref(dbus_pend);
    validate();
  }

  Return::~Return(){
    LOG("  ~ %s", O2P(*this));
  }
  
  Pending::Pending(Method &method_, Handle<Function> callback_)
    : method(method_), callback(callback_) {
    LOG("  * %s", O2P(*this));
    method.Ref();
  }

  Pending::~Pending(){
    LOG("  ~ %s", O2P(*this));
    method.Unref();
  }
  
  void
  Call::OnResult(DBusPendingCall *dbus_pend, void *data){
    Pending *pending = (Pending*)data;
    Handle<Function> callback = pending->callback;
    
    //HandleScope scope;
    
    Return res(pending->method, dbus_pend);
    
    LOG("    done %s", O2P(*pending));
    
    if(res.error != ""){
      CALL_ERROR(Error, res.error.c_str());
      return;
    }
    
    Local<Context> context = Context::GetCurrent();
    
    Handle<Value> args[2] = {
      Null(),
      res.decode()
    };
    
    TryCatch try_catch;
    
    callback->Call(context->Global(), 2, args);
    
    HANDLE_CAUGHT(try_catch);
  }
  
  void
  Call::OnClean(void *data){
    Pending *pending = (Pending*)data;
    delete pending;
  }
  
  Call::Call(Method &method_)
    : Outgoing(), method(method_), timeout(DBUS_TIMEOUT_USE_DEFAULT) {
    LOG("  * %s", O2P(*this));
    dbus_sign = method.arg.c_str();
    dbus_mesg = dbus_message_new_method_call
      (method.interface.service.c_str(), method.interface.path.c_str(),
       method.interface.name.c_str(), method.name.c_str());
  }

  Call::~Call(){
    LOG("  ~ %s", O2P(*this));
  }

  Handle<Value>
  Call::call(){
    Connection& connection(method.interface.connection);
    DBusPendingCall *dbus_pend;
    
    LOG("    do %s", O2P(*this));
    
    if(!dbus_connection_send_with_reply
       (connection.dbus_conn, dbus_mesg, &dbus_pend, timeout)){
      CALL_ERROR(Error, "error method call: Out of Memory");
      return Undefined();
    }
    
    if(!dbus_pend){
      CALL_ERROR(Error, "error method call: Connection is closed");
      return Undefined();
    }
    
    // blocking sending
    dbus_connection_flush(connection.dbus_conn);
    
    if(!callback.IsEmpty()){
      Pending *pending = new Pending(method, callback);
      
      if (!dbus_pending_call_set_notify
          (dbus_pend, OnResult, pending, OnClean)){
        delete pending;
        
        CALL_ERROR(Error, "error set pending: Out of Memory");
      }
    }
    
    return Undefined();
  }
  
  /* Method.call(args, callback[, timeout]); */
  Handle<Value> Method::DoCall(const Arguments& args){
    HandleScope scope;
    
    Method *method = ObjectWrap::Unwrap<Method>(args.This());

    if(!method){
      THROW_SCOPE(Error, "Method object corrupted.");
    }
    
    Call call(*method);
    
    unsigned count = args.Length();
    
    TryCatch try_catch;
    
    for(unsigned i = 0; i < count; i++){
      if(args[i]->IsArray()){
        call.encode(Handle<Array>::Cast(args[i]));
      }else if(args[i]->IsFunction()){
        call.callback = Persistent<Function>::New(Handle<Function>::Cast(args[i]));
      }else if(args[i]->IsUint32()){
        call.timeout = args[i]->Uint32Value();
      }
    }
    
    HANDLE_CAUGHT(try_catch)else{
      return scope.Close(call.call());
    }
    
    return scope.Close(Undefined());
  }

  /* Method.reg([callback]); */
  Handle<Value> Method::OnCall(const Arguments& args){
    HandleScope scope;
    
    Method *method = ObjectWrap::Unwrap<Method>(args.This());
    
    if(!method){
      THROW_SCOPE(Error, "Method object corrupted.");
    }
    
    if(args.Length() > 0){
      if(args[0]->IsFunction()){
        method->attach(Handle<Function>::Cast(args[0]));
        return scope.Close(Boolean::New(true));
      }else{
        method->detach();
        return scope.Close(Boolean::New(false));
      }
    }
    
    return scope.Close(method->handler);
  }
}
