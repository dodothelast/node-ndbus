#ifndef __SIGNAL_HH__
#define __SIGNAL_HH__

#include "common.hh"
#include "message.hh"
#include "interface.hh"

namespace ndbus {
  
  class Signal: public Member {
    friend class Emit;
    friend class Event;
  protected:
    string arg;
    
    Signal(Interface& interface, string name, string arg);
    ~Signal();
    
    string rule();
    //bool on(DBusMessage *msg);
    
    bool dispatch(DBusMessage *dbus_mesg);
  public:
    
    static void Init(Handle<Object> target);
    static Handle<Value> Construct(const Arguments& args);
    
    static Handle<Value> DoEmit(const Arguments& args);
    static Handle<Value> OnEmit(const Arguments& args);

    O2S("signal [%p] %s <%s | %s", this, name.c_str(), arg.c_str(), O2P(interface));
  };

  class Event: public Incoming {
    friend class Signal;
  protected:
    Signal& signal;
    
    Event(Signal& signal, DBusMessage *mesg);
  };
  
  class Emit: public Outgoing {
    friend class Signal;
  protected:
    Signal& signal;
    
    Emit(Signal& signal);
    ~Emit();
    
    Handle<Value> emit();
    
    O2S("emit [%p] of %s", this, O2P(signal));
  };
}

#endif//__SIGNAL_HH__
