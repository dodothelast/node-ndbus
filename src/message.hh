#ifndef __MESSAGE_HH__
#define __MESSAGE_HH__

#include "common.hh"

namespace ndbus{
  class Message {
  protected:
    const char *dbus_sign;
    DBusMessage *dbus_mesg;
    
  public:
    Message();
    Message(DBusMessage *mesg);
    ~Message();
    
    int sign_count()const;
    int mesg_count()const;
    
    O2S("message [%p]", this);
  };
  
  class Outgoing: public Message {
  protected:
    Handle<Value> encode(const Handle<Array> args);
  };
  
  class Incoming: public Message {
  public:
    Incoming();
    Incoming(DBusMessage *mesg);
    
  protected:
    string error;
    
    bool validate();
    
    Handle<Array> decode();
  };
}

#endif//__MESSAGE_HH__
