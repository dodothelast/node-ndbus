#include "interface.hh"

#include <map>

namespace ndbus {
  typedef map<string,Interface*> InterfaceMap;
  
  InterfaceMap iface_map;
  
  static string
  InterfaceKey(Connection& connection,
               string service,
               string path,
               string name){
    return connection.id() + service + path + name;
  }
  
  Interface::Interface(Connection& connection_,
                       string service_,
                       string path_,
                       string name_)
    : connection(connection_),
      service(service_),
      path(path_), name(name_) {
    LOG("* %s", O2P(*this));
    connection.Ref();
    iface_map[InterfaceKey(connection, service, path, name)] = this;
  }
  
  Interface::~Interface(){
    LOG("~ %s", O2P(*this));
    iface_map.erase(InterfaceKey(connection, service, path, name));
    connection.Unref();
  }
  
  void
  Interface::Init(Handle<Object> target){
    Local<FunctionTemplate> tpl = FunctionTemplate::New(Construct);
    
    tpl->SetClassName(String::NewSymbol("DBusInterface"));
    tpl->InstanceTemplate()->SetInternalFieldCount(1);
    
    target->Set(String::NewSymbol("Interface"), tpl->GetFunction());
  }
  
  /* new Interface(connection, "dom.Service.Name", "/path/to/node", "dom.Interface.Name"); */
  Handle<Value>
  Interface::Construct(const Arguments& args){
    HandleScope scope;
    
    Connection *connection = ObjectWrap::Unwrap<Connection>(args[0]->ToObject());
    
    String::Utf8Value service(args[1]->ToString());
    String::Utf8Value path(args[2]->ToString());
    String::Utf8Value name(args[3]->ToString());
    
    //InterfaceKey key(*connection, *service, *path, *name);
    
    InterfaceMap::iterator iter = iface_map.find(InterfaceKey(*connection, *service, *path, *name));
    
    if(iter != iface_map.end()){
      /* already exists */
      return scope.Close(iter->second->handle_);
    }else{
      /* new instance */
      Interface *iface = new Interface
        (*connection, *service, *path, *name);
      
      iface->Wrap(args.This());
      
      return scope.Close(args.This());
    }
  }
  
  Member::Member(Interface& interface_, string name_)
    : interface(interface_), name(name_){
    //LOG("  * %s", O2P(*this));
    interface.Ref();
  }
  
  Member::~Member(){
    //LOG("  ~ %s", O2P(*this));
    detach();
    interface.Unref();
  }
  
  string
  Member::rule(){
    return (interface.path.length() ? ",path='" + interface.path + "'" : "")
      + (interface.name.length() ? ",interface='" + interface.name + "'" : "")
      + (name.length() ? ",member='" + name + "'" : "");
  }
  
  DBusHandlerResult
  Member::handle_message(DBusConnection *dbus_conn, DBusMessage *dbus_mesg, void *user_data){
    Member *member = (Member*)user_data;
    
    return member->dispatch(dbus_mesg) ? DBUS_HANDLER_RESULT_HANDLED : DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
  }
  
  void
  Member::attach(Handle<Function> handler_){
    LOG("  attach %s", O2P(*this));
    
    DBusConnection *dbus_conn = interface.connection.dbus_conn;
    handler = Persistent<Function>::New(handler_);
    
    DBusErrorWrap dbus_err;
    string rule_ = rule();
    
    LOG("  * match %s", rule_.c_str());

    dbus_bus_add_match(dbus_conn, rule_.c_str(), &dbus_err);
    
    if(dbus_err){
      LOG("add match error: %s", *dbus_err);
      return;
    }
    
    dbus_connection_add_filter(dbus_conn, handle_message, (void*)this, NULL);
  }
  
  void
  Member::detach(){
    if(!handler.IsEmpty()){
      LOG("  detach %s", O2P(*this));
      
      DBusConnection *dbus_conn = interface.connection.dbus_conn;
      
      dbus_connection_remove_filter(dbus_conn, handle_message, (void*)this);
      
      DBusErrorWrap dbus_err;
      string rule_ = rule();
      
      LOG("  ~ match %s", rule_.c_str());
      
      dbus_bus_remove_match(dbus_conn, rule_.c_str(), &dbus_err);
      
      if(dbus_err){
        LOG("remove match error: %s", *dbus_err);
        return;
      }
      
      handler.Dispose();
      handler.Clear();
    }
  }

  bool
  Member::dispatch(DBusMessage *dbus_mesg){
    LOG("  # sender='%s',destination='%s',path='%s',interface='%s',member='%s'", dbus_message_get_sender(dbus_mesg), dbus_message_get_destination(dbus_mesg), dbus_message_get_path(dbus_mesg), dbus_message_get_interface(dbus_mesg), dbus_message_get_member(dbus_mesg));
    
    return interface.path.length() ? dbus_message_has_path(dbus_mesg, interface.path.c_str()) : true;
  }
}
