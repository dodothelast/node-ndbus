#include "message.hh"

namespace ndbus {
  static bool
  encode_key(Handle<Value> key, DBusMessageIter& mesg_iter, const char *sign){
    // TODO cast type of key to needed sign
    String::Utf8Value val(key->ToString());
    DBusBasicValue data;
    data.str = *val;
    
    dbus_message_iter_append_basic
      (&mesg_iter, DBUS_TYPE_STRING, &data);
    
    return true;
  }
  
  static bool
  encode_value(Handle<Value> value, DBusMessageIter& mesg_iter, const char *sign){
    DBusSignatureIter sign_iter;
    int dbus_type;
    
    dbus_signature_iter_init(&sign_iter, sign);
    
    switch(dbus_type = dbus_signature_iter_get_current_type(&sign_iter)){

#define APPEND_BASIC(_TYPE_, _type_, _def_, _v8g_)                      \
      case DBUS_TYPE_ ## _TYPE_: {                                      \
        _def_;                                                          \
        DBusBasicValue data;                                            \
        data._type_ = _v8g_;                                            \
        return dbus_message_iter_append_basic                           \
          (&mesg_iter, dbus_type, &data);                               \
      }

#define APPEND_NUMBER(_TYPE_, _type_, _v8type_)                 \
      APPEND_BASIC(_TYPE_, _type_, if(!value->Is##_v8type_()) return false;, value->_v8type_##Value())
      
      APPEND_NUMBER(BOOLEAN, bool_val, Boolean);
      
      APPEND_NUMBER(BYTE, byt, Uint32);
      
      APPEND_NUMBER(INT16, i16, Int32);
      APPEND_NUMBER(INT32, i32, Int32);
      APPEND_NUMBER(INT64, i64, Number);

      APPEND_NUMBER(UINT16, u16, Uint32);
      APPEND_NUMBER(UINT32, u32, Uint32);
      APPEND_NUMBER(UINT64, u64, Number);
      
      APPEND_NUMBER(DOUBLE, dbl, Number);
      
    case DBUS_TYPE_STRING:
    case DBUS_TYPE_OBJECT_PATH:
    case DBUS_TYPE_SIGNATURE: {
      if(!value->IsString()) return false;
      String::Utf8Value val(value->ToString());
      DBusBasicValue data;
      data.str = *val;
      return dbus_message_iter_append_basic(&mesg_iter, dbus_type, &data);
    }
      
    case DBUS_TYPE_STRUCT: {
      DBusMessageIter sub_mesg_iter;
      DBusSignatureIter sub_sign_iter;
      
      if(!dbus_message_iter_open_container
         (&mesg_iter, dbus_type, NULL, &sub_mesg_iter)){
        return false;
      }
      
      Local<Object> object = value->ToObject();
      Local<Value> prop;
      Local<Array> props = object->GetPropertyNames();
      int count = props->Length();
      char *sub_sign;
      
      dbus_signature_iter_recurse(&sign_iter, &sub_sign_iter);
      for(int i = 0;
          !i || dbus_signature_iter_next(&sub_sign_iter);
          i++){
        if(count <= i){
          dbus_message_iter_close_container(&mesg_iter, &sub_mesg_iter);
          return false;
        }
        
        sub_sign = dbus_signature_iter_get_signature(&sub_sign_iter);
        prop = props->Get(i);
        
        if(!encode_value(object->Get(prop), sub_mesg_iter, sub_sign)){
          dbus_free(sub_sign);
          dbus_message_iter_close_container(&mesg_iter, &sub_mesg_iter);
          return false;
        }
        
        dbus_free(sub_sign);
      }
      
      dbus_message_iter_close_container(&mesg_iter, &sub_mesg_iter);
      return true;
    }

    case DBUS_TYPE_ARRAY: {
      if(dbus_signature_iter_get_element_type(&sign_iter) == DBUS_TYPE_DICT_ENTRY){
        if(!value->IsObject()){
          return false;
        }
        
        Local<Object> object = value->ToObject();
        DBusMessageIter sub_mesg_iter;
        DBusSignatureIter sub_sign_iter, pair_sign_iter;
        char *sub_sign;

        dbus_signature_iter_recurse(&sign_iter, &sub_sign_iter);
        sub_sign = dbus_signature_iter_get_signature(&sub_sign_iter);

        if(!dbus_message_iter_open_container
           (&mesg_iter, DBUS_TYPE_ARRAY, sub_sign, &sub_mesg_iter)){
          dbus_free(sub_sign);
          return false;
        }
        dbus_free(sub_sign);

        Local<Array> props = object->GetPropertyNames();
        int count = props->Length();
        
        //for each signature
        dbus_signature_iter_recurse(&sub_sign_iter, &pair_sign_iter); //the key
        
        char *key_sign = dbus_signature_iter_get_signature(&pair_sign_iter);
        
        dbus_signature_iter_next(&pair_sign_iter); //the value
        
        char *val_sign = dbus_signature_iter_get_signature(&pair_sign_iter);
        
        for(int i = 0; i < count; i++){
          Local<Value> prop = props->Get(i);
          DBusMessageIter dict_iter;
          
          if(!(dbus_message_iter_open_container
               (&sub_mesg_iter, DBUS_TYPE_DICT_ENTRY, NULL, &dict_iter) &&
               //append the key and val
               encode_key(prop, dict_iter, key_sign) &&
               encode_value(object->Get(prop), dict_iter, val_sign))){
            
            dbus_message_iter_close_container(&sub_mesg_iter, &dict_iter);
            
            dbus_free(key_sign);
            dbus_free(val_sign);
            dbus_message_iter_close_container(&mesg_iter, &sub_mesg_iter);
            return false;
          }
          
          dbus_message_iter_close_container(&sub_mesg_iter, &dict_iter);
        }
        
        dbus_free(key_sign);
        dbus_free(val_sign);
        dbus_message_iter_close_container(&mesg_iter, &sub_mesg_iter);
        return true;
      }else{
        if(!value->IsArray()){
          return false;
        }
        
        DBusMessageIter sub_mesg_iter;
        DBusSignatureIter sub_sign_iter;
        char *sub_sign = NULL;

        dbus_signature_iter_recurse(&sign_iter, &sub_sign_iter);
        sub_sign = dbus_signature_iter_get_signature(&sub_sign_iter);
        
        if(!dbus_message_iter_open_container
           (&mesg_iter, DBUS_TYPE_ARRAY, sub_sign, &sub_mesg_iter)){
          dbus_free(sub_sign);
          return false;
        }
        
        Handle<Array> array = Handle<Array>::Cast(value);
        uint32_t count = array->Length();
        
        for(uint32_t i = 0; i < count; i++){
          Handle<Value> entry = array->Get(i);
          if(!encode_value(entry, sub_mesg_iter, sub_sign)){
            dbus_message_iter_close_container(&mesg_iter, &sub_mesg_iter);
            dbus_free(sub_sign);
            return false;
          }
        }
        dbus_message_iter_close_container(&mesg_iter, &sub_mesg_iter);
        dbus_free(sub_sign);
        
        return true;
      }
    }
      
    case DBUS_TYPE_VARIANT: {
      DBusMessageIter sub_mesg_iter;
      DBusSignatureIter sub_sign_iter;
      
      const char *sub_sign = NULL;

      if(value->IsTrue() ||
         value->IsFalse() ||
         value->IsBoolean()){
        sub_sign = DBUS_TYPE_BOOLEAN_AS_STRING;
      }else if(value->IsInt32()){
        sub_sign = DBUS_TYPE_INT32_AS_STRING;
      }else if(value->IsUint32()){
        sub_sign = DBUS_TYPE_UINT32_AS_STRING;
      }else if(value->IsNumber()){
        sub_sign = DBUS_TYPE_DOUBLE_AS_STRING;
      }else if(value->IsString()){
        sub_sign = DBUS_TYPE_STRING_AS_STRING;
      }else if(value->IsArray()){
        sub_sign = DBUS_TYPE_ARRAY_AS_STRING;
      }
      
      dbus_signature_iter_recurse(&sign_iter, &sub_sign_iter);
      
      if(!dbus_message_iter_open_container
         (&mesg_iter, DBUS_TYPE_VARIANT, sub_sign, &sub_mesg_iter)){
        return false;
      }

      //encode the object to dbus message
      if(!encode_value(value, sub_mesg_iter, sub_sign)) {
        dbus_message_iter_close_container(&mesg_iter, &sub_mesg_iter);
        return false;
      }
      dbus_message_iter_close_container(&mesg_iter, &sub_mesg_iter);
      return true;
    }
      
    }
    return false;
  }
  
  Handle<Value>
  Outgoing::encode(const Handle<Array> args){
    DBusMessageIter mesg_iter;    
    dbus_message_iter_init_append(dbus_mesg, &mesg_iter);
    
    int needs = sign_count();
    int count = args->Length();
    
    if(count < needs){
      RET_ERROR(TypeError, "Arguments not enough");
    }

    if(count > needs){
      RET_ERROR(TypeError, "Too many arguments");
    }

    char *sign;

    DBusSignatureIter sign_iter;
    int i;
    
    for(i = 0, dbus_signature_iter_init(&sign_iter, dbus_sign);
        i < count; i++, dbus_signature_iter_next(&sign_iter)){
      
      sign = dbus_signature_iter_get_signature(&sign_iter);
      
      if(!encode_value(args->Get(i), mesg_iter, sign)){
        ostringstream msg_;
        
        msg_ << "Argument " << i << " doesn't match to its signature: " << sign;

        string msg(msg_.str());
        
        dbus_free(sign);
        
        RET_ERROR(TypeError, msg.c_str());
      }
      
      dbus_free(sign);
    }
    
    return Undefined();
  }
}
