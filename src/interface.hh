#ifndef __INTERFACE_HH__
#define __INTERFACE_HH__

#include "common.hh"
#include "connection.hh"

namespace ndbus {
  class Interface: public ObjectWrap {
    friend class Member;
  public:
    Connection& connection;
    string service;
    string path;
    string name;
    
  protected:
    Interface(Connection& connection,
              string service,
              string path,
              string name);
    ~Interface();
    
  public:
    static void Init(Handle<Object> target);
    static Handle<Value> Construct(const Arguments& args);

    O2S("interface [%p] %s %s %s | %s", this, service.c_str(), path.c_str(), name.c_str(), O2P(connection));
  };

  class Member: public ObjectWrap {
  protected:
    Persistent<Function> handler;
    
    virtual string rule();
    
    void attach(Handle<Function> handler);
    void detach();
    
    static DBusHandlerResult handle_message(DBusConnection *dbus_conn, DBusMessage *dbus_mesg, void *user_data);
    virtual bool dispatch(DBusMessage *dbus_mesg);
  public:
    Interface &interface;
    string name;
    
    Member(Interface& interface, string name);
    ~Member();
    
    O2S("member [%p] name [%s] of %s", this, name.c_str(), O2P(interface));
  };
}

#endif//__INTERFACE_HH__
