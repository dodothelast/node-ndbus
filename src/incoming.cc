#include "message.hh"

namespace ndbus {
  Incoming::Incoming(): Message() {
    
  }
  
  Incoming::Incoming(DBusMessage *mesg): Message(mesg) {
    
  }
  
  bool
  Incoming::validate(){
    DBusErrorWrap dbus_err;
    
    if(dbus_set_error_from_message(&dbus_err, dbus_mesg)){
      error = "Error reply: ";
      error += *dbus_err;
      
      return false;
    }
        
    if(dbus_message_get_type(dbus_mesg) == DBUS_MESSAGE_TYPE_ERROR){
      error = "Error reply: ";
      error += dbus_message_get_error_name(dbus_mesg);
      return false;
    }

    return true;
  }
  
  static Handle<Value>
  decode_value(DBusMessageIter& mesg_iter) {
    switch(dbus_message_iter_get_arg_type(&mesg_iter)){

#define PARSE_BASIC(_TYPE_, _type_, _v8type_)           \
      case DBUS_TYPE_##_TYPE_: {                        \
        DBusBasicValue data;                            \
        dbus_message_iter_get_basic(&mesg_iter, &data); \
        return _v8type_::New(data._type_);              \
      }
      
      PARSE_BASIC(BOOLEAN, bool_val, Boolean);
      
      PARSE_BASIC(BYTE, byt, Number);

      PARSE_BASIC(INT16, i16, Number);
      PARSE_BASIC(INT32, i32, Number);
      PARSE_BASIC(INT64, i64, Number);

      PARSE_BASIC(UINT16, u16, Number);
      PARSE_BASIC(UINT32, u32, Number);
      PARSE_BASIC(UINT64, u64, Number);

      PARSE_BASIC(DOUBLE, dbl, Number);
      
    case DBUS_TYPE_OBJECT_PATH:
    case DBUS_TYPE_SIGNATURE:
    case DBUS_TYPE_STRING: {
      DBusBasicValue data;
      dbus_message_iter_get_basic(&mesg_iter, &data);
      return String::New(data.str);
    }
      
    case DBUS_TYPE_ARRAY:
    case DBUS_TYPE_STRUCT: {
      DBusMessageIter sub_mesg_iter;
      
      dbus_message_iter_recurse(&mesg_iter, &sub_mesg_iter);
      
      if(dbus_message_iter_get_arg_type(&sub_mesg_iter) == DBUS_TYPE_DICT_ENTRY){
        // dict
        
        Local<Object> object = Object::New();
        DBusMessageIter pair_iter;
        
        do{
          // key
          dbus_message_iter_recurse(&sub_mesg_iter, &pair_iter);
          Handle<Value> key = decode_value(pair_iter);
          // value
          dbus_message_iter_next(&pair_iter);
          Handle<Value> value = decode_value(pair_iter);
          
          object->Set(key, value);
        }while(dbus_message_iter_next(&sub_mesg_iter));
        
        return object;
      }else{
        // array
        
        uint32_t count = 0;
        
        for(;
            dbus_message_iter_get_arg_type(&sub_mesg_iter) != DBUS_TYPE_INVALID;
            count++){
          dbus_message_iter_next(&sub_mesg_iter);
        }
        
        dbus_message_iter_recurse(&mesg_iter, &sub_mesg_iter);
        
        Local<Array> array = Array::New(count);
        count = 0;
        
        do{
          array->Set(count++, decode_value(sub_mesg_iter));
        }while(dbus_message_iter_next(&sub_mesg_iter));
        
        if(count == 1){
          if(array->Get(0)->IsUndefined()){
            return Array::New(0);
          }
        }
        
        return array;
      }
    }
      
    case DBUS_TYPE_VARIANT: {
      DBusMessageIter sub_mesg_iter;
      dbus_message_iter_recurse(&mesg_iter, &sub_mesg_iter);
      
      return decode_value(sub_mesg_iter);
    }
      
    }
    
    return Undefined();
  }
  
  Handle<Array>
  Incoming::decode(){
    if(dbus_sign && !dbus_message_has_signature(dbus_mesg, dbus_sign)){
      THROW_ERROR(Error, "Message signature mismatch.");
    }
    
    int count = mesg_count();
    Local<Array> result = Array::New(count);
    
    DBusMessageIter mesg_iter;
    int i;
    
    for(i = 0, dbus_message_iter_init(dbus_mesg, &mesg_iter);
        i < count; i++, dbus_message_iter_next(&mesg_iter)){
      result->Set(i, decode_value(mesg_iter));
    }
    
    return result;
  }
}
