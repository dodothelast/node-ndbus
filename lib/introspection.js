var XML = require('./colibrixml'),

    doctype = '<!DOCTYPE node PUBLIC "-//freedesktop//DTD D-BUS Object Introspection 1.0//EN"\n"http://www.freedesktop.org/standards/dbus/1.0/introspect.dtd">';

function parse_method_args(nodes, method){
  var i = 0,
      node,
      dirt,
      attr;

  for(; i < nodes.length; ){
    node = nodes[i++];
    attr = node.attr;
    switch(node.name){
      case 'arg':
      dirt = attr.direction === 'out' ? 'res' : 'arg';
      (method[dirt] = method[dirt] || {})[attr.name || ('arg' + i)] = attr.type;
      break;
      case 'annotation':
      (method.$ = method.$ || {})[attr.name] = attr.value;
      break;
    }
  }
}

function parse_signal_args(nodes, signal){
  var i = 0,
      node,
      attr;

  for(; i < nodes.length; ){
    node = nodes[i++];
    attr = node.attr;
    switch(node.name){
      case 'arg':
      (signal.arg = signal.arg || {})[attr.name || ('arg' + i)] = attr.type;
      break;
      case 'annotation':
      (signal.$ = signal.$ || {})[attr.name] = attr.value;
      break;
    }
  }
}

function parse_attrib_ants(nodes, attrib){
  var i = 0,
      node,
      attr;

  for(; i < nodes.length; ){
    node = nodes[i++];
    attr = node.attr;
    switch(node.name){
      case 'annotation':
      (attrib.$ = attrib.$ || {})[attr.name] = attr.value;
      break;
    }
  }
}

function parse_interface(nodes, iface){
  var i = 0,
      node,
      attr;

  for(; i < nodes.length; ){
    node = nodes[i++];
    attr = node.attr;
    switch(node.name){
      case 'method':
      parse_method_args(node.node, (iface.method = iface.method || {})[attr.name] = {});
      break;
      case 'signal':
      parse_signal_args(node.node, (iface.signal = iface.signal || {})[attr.name] = {});
      break;
      case 'property':
      parse_attrib_ants(node.node, (iface.attrib = iface.attrib || {})[attr.name] = {
        type: attr.type,
        read: attr.access.indexOf('read') >= 0,
        write: attr.access.indexOf('write') >= 0
      });
      break;
      case 'annotation':
      (iface.$ = iface.$ || {})[attr.name] = attr.value;
      break;
    }
  }
}

function parse_interfaces(nodes, object){
  var i = 0,
      node;

  for(; i < nodes.length; ){
    node = nodes[i++];
    if(node.name == 'interface'){
      parse_interface(node.node, object[node.attr.name] = {});
    }
  }
}

function parse_object(nodes, object, objects, path){
  parse_interfaces(nodes, object);
  parse_objects(nodes, objects, path);
}

function parse_objects(nodes, objects, root){
  var i = 0,
      node,
      path;

  for(; i < nodes.length; ){
    node = nodes[i++];
    if(node.name == 'node'){
      path = node.attr.name || '';
      if(root){
        path = root + '/' + path;
      }
      parse_object(node.node, objects[path] = {}, objects, path);
    }
  }
}

function parse(xml){
  var objects = {},
      nodes = XML.parse(xml);

  parse_objects(nodes, objects);

  return objects;
}

function format_ants(subject, nodes){
  var ants = subject.$,
      name;

  if(ants){
    for(name in ants){
      nodes.push({
        name: 'annotation',
        attr: {
          name: name,
          value: ants[name]
        },
        node: []
      });
    }
  }
}

function format_method_args(method, nodes){
  var name;

  if(method.arg){
    for(name in method.arg){
      nodes.push({
        name: 'arg',
        attr: {
          name: name,
          type: method.arg[name],
          direction: 'in'
        },
        node: []
      });
    }
  }

  if(method.res){
    for(name in method.res){
      nodes.push({
        name: 'arg',
        attr: {
          name: name,
          type: method.res[name],
          direction: 'out'
        },
        node: []
      });
    }
  }

  format_ants(method, nodes);
}

function format_methods(methods, nodes){
  var name,
      method;

  if(methods){
    for(name in methods){
      nodes.push(method = {
        name: 'method',
        attr: {
          name: name
        },
        node: []
      });
      format_method_args(methods[name], method.node);
    }
  }
}

function format_signal_args(signal, nodes){
  var name;

  if(signal.arg){
    for(name in signal.arg){
      nodes.push({
        name: 'arg',
        attr: {
          name: name,
          type: signal.arg[name]
        },
        node: []
      });
    }
  }

  format_ants(signal, nodes);
}

function format_signals(signals, nodes){
  var name,
      signal;

  if(signals){
    for(name in signals){
      nodes.push(signal = {
        name: 'signal',
        attr: {
          name: name
        },
        node: []
      });
      format_signal_args(signals[name], signal.node);
    }
  }
}

function format_attribs(attribs, nodes){
  var name,
      subnodes,
      attrib;

  if(attribs){
    for(name in attribs){
      attrib = attribs[name];
      nodes.push({
        name: 'property',
        attr: {
          name: name,
          type: attrib.type,
          access: (attrib.read ? 'read' : '') + (attrib.write ? 'write' : '')
        },
        node: subnodes = []
      });
      format_ants(attrib, subnodes);
    }
  }
}

function format_objects(objects, nodes){
  var path,
      object,
      name,
      subnodes,
      ifaces;

  for(path in objects){
    nodes.push(object = {
      name: 'node',
      attr: {},
      node: []
    });
    if(path){
      object.attr.name = path;
    }
    ifaces = objects[path];
    for(name in ifaces){
      object.node.push({
        name: 'interface',
        attr: {
          name: name
        },
        node: subnodes = []
      });
      format_methods(ifaces[name].method, subnodes);
      format_signals(ifaces[name].signal, subnodes);
      format_attribs(ifaces[name].attrib, subnodes);
      format_ants(ifaces[name], subnodes);
    }
  }
}

function shift_nodes(nodes){
  var i = 0,
      name,
      node,
      cnode,
      path,
      pnode,
      node_by_path = {};

  for(; i < nodes.length; ){
    node = nodes[i++];
    node_by_path[node.attr.name || ''] = node;
  }

  for(i = 0; i < nodes.length; i++){
    cnode = nodes[i];
    name = cnode.attr.name;
    if(!name) continue;
    pnode = null;
    for(path in node_by_path){
      node = node_by_path[path];
      if(name.indexOf(path + (path != '' && path != '/' ? '/' : '')) === 0 && name !== path &&
        (!pnode || path.length > (pnode.attr.name || '').length)){
        pnode = node;
      }
    }
    if(pnode){
      nodes.splice(i--, 1);
      if(pnode.attr.name){
        cnode.attr.name = cnode.attr.name.split(pnode.attr.name + '/')[1];
      }
      pnode.node.push(cnode);
    }
  }
}

function format(objects, indent){
  var nodes = [];

  format_objects(objects, nodes);
  shift_nodes(nodes);

  return XML.format(nodes, indent, doctype);
}

exports.parse = parse;
exports.format = format;
