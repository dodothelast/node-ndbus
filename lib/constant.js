module.exports = {
  Service: {
    DBus: "org.freedesktop.DBus"
  },

  Path: {
    Root: "/",
    DBus: "/org/freedesktop/DBus",
    Local: "/org/freedesktop/DBus/Local"
  },

  Interface: {
    DBus: "org.freedesktop.DBus",
    Introspectable: "org.freedesktop.DBus.Introspectable",
    Properties: "org.freedesktop.DBus.Properties",
    Peer: "org.freedesktop.DBus.Peer"
  },

  Annotation: {
    Deprecated:       'org.freedesktop.DBus.Deprecated',
    CSymbol:          'org.freedesktop.DBus.GLib.CSymbol',
    NoReply:          'org.freedesktop.DBus.Method.NoReply',
    EmitChanged:      'org.freedesktop.DBus.Property.EmitsChangedSignal'
  },

  RequestNameFlag: {
    AllowReplacement: 0x1,
    ReplaceExisting:  0x2,
    DoNotQueue:       0x4
  },
  RequestNameReply: {
    PrimaryOwner:     1,
    InQueue:          2,
    Exists:           3,
    AlreadyOwner:     4
  },

  ReleaseNameReply: {
    Released:         1,
    NonExistent:      2,
    NotOwner:         3
  },

  StartReply: {
    Success:          1,
    AlreadyRunning:   2
  }
};
