{
  'path/to/object': {
    'name.of.interface': {
      method: {
        MethodName: {
          arg: {
            argName: 'argType'
          },
          res: {
            resultName: 'resultType'
          },
          $: {
            'name.of.annotation': 'annotationValue'
          }
        }
      },
      signal: {
        SignalName: {
          arg: {
            argName: 'argType'
          },
          $: {
            'name.of.annotation': 'annotationValue'
          }
        }
      },
      attrib: {
        PropertyName: {
          type: 'propertyType',
          read: true,
          write: false,
          $: {
            'name.of.annotation': 'annotationValue'
          }
        }
      },
      $: {
        'name.of.annotation': 'annotationValue'
      }
    }
  }
}