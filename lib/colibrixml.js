/**
 * Simple Ultra Lightweight XML Parser/Formatter
 */

function parse_attrs(str, attrs){
  (function(_, name, value, after){
    if(!_) return;
    attrs[name] = value;
    str = parse_attrs(after, attrs);
  }).apply(this, str.match(/^\s*([a-zA-Z0-9_-]+)\s*=\s*"([^"]*)"([\S\s]*)$/));
  return str;
}

function parse_nodes(str, nodes){
  (function(_, name, after){
    if(!_) return;
    var node = {
      name: name,
      attr: {},
      node: []
    };
    nodes.push(node);
    str = parse_attrs(after, node.attr);
    (function(_, closed, after){
      if(!_) return;
      str = after;
      if(!closed){ // parse subnodes
        str = parse_nodes(after, node.node);
        (function(_, name, after){
          if(!_) return;
          // skip close tag
          str = after;
        }).apply(this, str.match(/^\s*<\/([a-zA-Z0-9_-]+)>([\S\s]*)$/));
      }
      str = parse_nodes(str, nodes);
    }).apply(this, str.match(/^\s*(\/?)>([\S\s]*)$/));
  }).apply(this, str.match(/^\s*<([a-zA-Z0-9_-]+)([\S\s]*)$/));
  return str;
}

function parse(str){
  var nodes = [];
  // skip doctype

  (function(_, str){
    if(!_) return;
    parse_nodes(str, nodes);
  }).apply(this, str.match(/^(?:\s*<!(?:[^>]|\s)+>)([\S\s]*)$/));

  return nodes;
}

function format_attrs(attrs){
  var str = '';
  for(var name in attrs){
    str += ' ' + name + '="' + attrs[name] + '"';
  }
  return str;
}

function format_nodes(nodes, indent, _indent){
  var i = 0,
      node,
      ind = indent ? _indent : '',
      sep = indent ? '\n' : '',
      str = '';
  for(; i < nodes.length; ){
    node = nodes[i];
    str += (i++ ? sep : '') + ind + '<' + node.name + format_attrs(node.attr) + (node.node.length ? ('>' + sep + format_nodes(node.node, indent, _indent + indent) + sep + ind + '</' + node.name + '>') : '/>');
  }
  return str;
}

function format(obj, indent, doct){
  return (doct ? doct + (indent ? '\n' : '') : '') + format_nodes(obj, indent, '') + (format ? '\n' : '');
}

exports.parse = parse;
exports.format = format;
