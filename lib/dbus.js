var slice = Array.prototype.slice;

function dummy(){}

function addon(){
  var i = 0,
      errors = [];
  for(; i < arguments.length; ){
    try{
      return require(arguments[i++]);
    }catch(e){
      errors.push(e.message);
    }
  }
  throw new Error('Unable to load addon:\n' + errors.join('\n'));
}

var nDBus = addon('../build/Release/ndbus', '../build/Debug/ndbus'),
    Introspection = require('./introspection'),
    Constant = require('./constant');

/*
 * Proxy interface
 */

function getSignature(args){
  var key,
      sign = '';
  if(args){
    for(key in args){
      sign += args[key];
    }
  }
  return sign;
}

function proxyMethod(iface, name, intro){
  var method = new nDBus.Method(iface, name, getSignature(intro.arg), getSignature(intro.res));

  function call(){
    var args = slice.call(arguments),
        timeout,
        callback;

    if('function' === typeof args[args.length - 1]){
      callback = args[args.length - 1];
      args.splice(args.length - 1, 1);
    }else if('function' === typeof args[args.length - 2] &&
      'number' === typeof args[args.length - 1]){
      callback = args[args.length - 2];
      timeout = args[args.length - 1];
      args.splice(args.length - 2, 2);
    }
    method.call(args, callback && function(err, rets){
      if(err){
        callback(err);
        return;
      }
      rets.unshift(null);
      callback.apply(this, rets);
    }, callback && timeout);
  }

  return call;
}

function proxySignal(iface, name, intro){
  var signal = new nDBus.Signal(iface, name, getSignature(intro.arg)),
      callbacks = [];

  function notify(args){
    for(var i = 0; i < callbacks.length; i++){
      callbacks[i].apply(this, args);
    }
  }

  function event(){
    signal.emit(slice.call(arguments));
  };

  event.on = function(callback){
    if('function' === typeof callback){
      callbacks.push(callback);
      signal.reg(notify);
    }
  }

  event.off = function(callback){
    if('function' === typeof callback){
      var i = callbacks.indexOf(callback);
      if(i >= 0){
        callbacks.splice(i, 1);
        if(!callbacks.length){
          signal.reg(null);
        }
      }
    }else{
      callbacks = [];
      signal.reg(null);
    }
  }

  event.once = function(callback){
    if('function' === typeof callback){
      event.on(function(){
        callback.apply(this, arguments);
        event.off(callback);
      });
    }
  }

  return event;
}

function assertValues(values, callback){
  var i = 0,
      errors = [],
      type,
      value,
      state;

  for(; i < values.length; ){
    type = values[i++];
    value = values[i++];

    state = nDBus.Validate(type, value);

    if(state){
      errors.push(state);
    }
  }

  if(errors.length){
    callback(new Error('Invalid arguments:\n' + errors.join('\n')));
    return false;
  }

  return true;
}

function assertSignatures(name, member, callback){
  var errors = [];

  function check(args){
    var arg,
        type,
        state;

    for(arg in args){
      type = args[arg];

      state = nDBus.Validate('s', type);

      if(state){
        errors.push('Argument ' + arg + ': ' + state);
      }
    }
  }

  if(member.arg){
    check(member.arg);
  }

  if(member.res){
    check(member.res);
  }

  if(member.type){
    var state = nDBus.Validate('s', member.type);

    if(state){
      errors.push('Attrib: ' + state);
    }
  }

  if(errors.length){
    callback(new Error('Invalid signatures for ' + name + ':\n' + errors.join('\n')));
    return false;
  }

  return true;
}


function Proxy(){}

function getIntrospection(bus, service_name, node_path, callback){
  var $iface = new nDBus.Interface(bus, service_name, node_path, Constant.Interface.Introspectable),
      $introspect = new nDBus.Method($iface, 'Introspect', '', 's');

  $introspect.call(function(err, res){
    if(err){
      callback(err);
      return;
    }

    if(res.length < 1 || 'string' != typeof res[0]){
      callback(new Error('Introspection data unavailable.'));
      return;
    }

    try{
      res = Introspection.parse(res[0].replace('<?xml version="1.0"?>', ""));
    }catch(err){
      callback(err);
      return;
    }

    callback(null, res);
  });
}

function createProxy(bus, service_name, node_path, iface_name, callback){
  if('function' !== typeof callback){
    throw new Error('Result callback are required.');
  }

  if(!assertValues([
    'b', service_name,
    'p', node_path,
    'i', iface_name
  ], callback)){
    return;
  }

  getIntrospection(bus, service_name, node_path, function(err, $){
    if(err){
      callback(err);
      return;
    }

    var path_spec = $[''] || $[node_path];
    if(!path_spec){
      callback(new Error('Path ' + node_path + ' unavailable.'));
      return;
    }

    var iface_spec = path_spec[iface_name];

    if(!iface_spec){
      callback(new Error('Interface ' + iface_name + ' unavailable.'));
      return;
    }

    var iface = new nDBus.Interface(bus, service_name, node_path, iface_name),
        proxy = new Proxy(),

        name,
        methods = iface_spec.method || {},
        signals = iface_spec.signal || {};

    for(name in methods){
      proxy[name] = proxyMethod(iface, name, methods[name]);
    }

    for(name in signals){
      proxy[name] = proxySignal(iface, name, signals[name]);
    }

    proxy.$ = function(callback){
      for(name in methods){
        delete proxy[name];
      }
      for(name in signals){
        proxy[name].off();
        delete proxy[name];
      }
      callback && callback(null);
    };

    callback(null, proxy);
  });
}

/*
 * Service interface
 */

function serviceMethod(iface, name, intro){
  var method = new nDBus.Method(iface, name, getSignature(intro.arg), getSignature(intro.res));

  return function(callback){
    if('function' === typeof callback){
      method.reg(function(args, onres){
        args.push(function(){
          onres(slice.call(arguments));
        });
        return callback.apply(this, args);
      });
    }else{
      method.reg(null);
    }
  };
}

function serviceSignal(iface, name, intro){
  var signal = new nDBus.Signal(iface, name, getSignature(intro.arg));

  return function(){
    signal.emit(slice.call(arguments));
  };
}


function Interface(){}

function createInterface($, dbus, bus, service_name, node_path, iface_name, iface_spec, callback){
  if('function' !== typeof callback){
    throw new Error('Result callback are required.');
  }

  if(!assertValues([
    'i', iface_name
  ], callback)){
    return;
  }

  if('object' !== typeof iface_spec){
    throw new Error('Interface specification are required.');
  }

  var intern_iface = new nDBus.Interface(bus, service_name, node_path, iface_name),
      iface = new Interface(),

      name,
      methods = iface_spec.method || {},
      signals = iface_spec.signal || {};

  for(name in methods){
    if(!assertSignatures(name, methods[name], callback)){
      return;
    }
    iface[name] = serviceMethod(intern_iface, name, methods[name]);
  }

  for(name in signals){
    if(!assertSignatures(name, signals[name], callback)){
      return;
    }
    iface[name] = serviceSignal(intern_iface, name, signals[name]);
  }

  iface.$ = function(callback){
    delete $[node_path][iface_name];

    for(name in methods){
      iface[name]();
    }
    for(name in iface){
      delete iface[name];
    }

    callback && callback(null);
  };

  $[node_path][iface_name] = iface_spec;

  callback(null, iface);
}

function subPath(base, path){
  base = base.split('/');
  path = path.split('/');

  for( ; base.length > 1 && base[base.length - 1] == ''; base.pop());
  for( ; path.length > 1 && path[path.length - 1] == ''; path.pop());

  for(var i = 0; i < base.length; i++){
    if(path[i] != base[i]){
      return false;
    }
  }

  path = path.slice(i);

  if(path.length > 1){
    return false;
  }

  return path[0];
}

function Node(){}

function createNode($, dbus, bus, service_name, node_path, callback){
  if('function' !== typeof callback){
    throw new Error('Result callback are required.');
  }

  if(!assertValues([
    'p', node_path
  ], callback)){
    return;
  }

  var $iface = new nDBus.Interface(bus, service_name, node_path, Constant.Interface.Introspectable),
      $introspect = new nDBus.Method($iface, 'Introspect', '', 's');

  $introspect.reg(function(){
    var $$ = {};
    for(var path in $){
      if(path === node_path){
        $$[''] = $[path];
      }else{
        var subpath = subPath(node_path, path);
        if(subpath){
          $$[subpath] = {};
        }
      }
    }
    return [Introspection.format($$, '  ')];
  });

  $[node_path] = {};
  $[node_path][Constant.Interface.Introspectable] = {
    method: {
      Introspect: {
        res: {
          xml_data: 's'
        }
      }
    },
    _: $introspect // preserve from gc
  };

  var node = new Node();

  node.iface = function(iface_name, iface_spec, callback){
    createInterface($, dbus, bus, service_name, node_path, iface_name, iface_spec, callback);
  };

  node.$ = function(callback){
    delete $[node_path];
    callback && callback(null);
  };

  callback(null, node);
}


function Service(){}

function createService($, bus, service_name, callback){
  if('function' !== typeof callback){
    throw new Error('Result callback are required.');
  }

  if(!assertValues([
    'b', service_name
  ], callback)){
    return;
  }

  createProxy(bus, Constant.Service.DBus, Constant.Path.DBus, Constant.Interface.DBus, function(err, dbus){
    if(err){
      callback(err);
      return;
    }

    dbus.RequestName(service_name, Constant.RequestNameFlag.AllowReplacement, function(err, reply){
      if(err){
        callback(err);
        return;
      }

      var service = new Service();

      service.node = function(node_path, callback){
        createNode($, dbus, bus, service_name, node_path, callback);
      };

      service.$ = function(callback){
        dbus.ReleaseName(service_name, function(err, reply){
          if(err){
            callback(err);
            return;
          }

          callback(null);
        });
      };

      callback(null, service);
    });
  });
}


/*
 * Bus interface
 */

function Connection(){}

function createConnection(id){
  var bus = new nDBus.Connection(id),
      conn = new Connection();

  conn.explore = function(service_name, node_path, callback){
    getIntrospection(bus, service_name, node_path, callback);
  };

  conn.proxy = function(service_name, node_path, iface_name, callback){
    createProxy(bus, service_name, node_path, iface_name, callback);
  };

  conn.service = function(service_name, callback){
    createService({}, bus, service_name, callback);
  };

  return conn;
}

/*
 * Export section
 */

exports = module.exports = createConnection;

exports.Proxy = Proxy;

exports.Interface = Interface;
exports.Node = Node;
exports.Service = Service;

exports.internal = nDBus;

exports.version = nDBus.Version();
exports.machine = nDBus.Machine();

exports.introspection = Introspection;
exports.constant = Constant;
