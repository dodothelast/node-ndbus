var FS = require('fs'),
    XML = require('../lib/colibrixml'),

    doctype = '<!DOCTYPE node PUBLIC "-//freedesktop//DTD D-BUS Object Introspection 1.0//EN"\n"http://www.freedesktop.org/standards/dbus/1.0/introspect.dtd">',

    assert = require('assert'),

    tests = [
      'simple',
      'org.bluez',
      'org.ofono',
      'org.ofono.modem'
    ];

for(var i = 0; i < tests.length; i++){
  test(tests[i]);
}

function test(name){
  var source_xml = FS.readFileSync(__dirname + '/introspection/' + name + '.xml', 'utf8'),
      source_jso = JSON.parse(FS.readFileSync(__dirname + '/introspection/' + name + '.json', 'utf8')),
      parsed_jso,
      rendered_xml;

  console.log('test', name);

  parsed_jso = XML.parse(source_xml);

  try{
    assert.deepEqual(parsed_jso, source_jso);
    console.log('  parsing is ok');
  }catch(e){
    console.error('Parsed data of ' + name + '.xml is unexpected\n', e);
  }

  rendered_xml = XML.format(source_jso, '  ', doctype)

  try{
    assert.strictEqual(rendered_xml, source_xml);
    console.log('  formatting is ok');
  }catch(e){
    console.error('Rendered xml of ' + name + '.json is unexpected\n', e);
  }
}
