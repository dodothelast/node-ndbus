{
  "/org/freedesktop/sample_object": {
    "org.freedesktop.SampleInterface": {
      "method": {
        "Frobate": {
          "arg": {
            "foo": "i"
          },
          "res": {
            "bar": "s",
            "baz": "a{us}"
          },
          "$": {
            "org.freedesktop.DBus.Deprecated": "true"
          }
        },
        "Bazify": {
          "arg": {
            "bar": "(iiu)"
          },
          "res": {
            "bar": "v"
          }
        },
        "Mogrify": {
          "arg": {
            "bar": "(iiav)"
          }
        }
      },
      "signal": {
        "Changed": {
          "arg": {
            "new_value": "b"
          }
        }
      },
      "attrib": {
        "Bar": {
          "type": "y",
          "read": true,
          "write": true
        }
      }
    }
  },
  "/org/freedesktop/sample_object/child_of_sample_object": {},
  "/org/freedesktop/sample_object/another_child_of_sample_object": {}
}
