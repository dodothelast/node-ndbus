var DBus = require('..'),
    assert = require('assert'),

    session = DBus();

console.log('dbus', DBus.version.join('.'), 'on', DBus.machine);

session.service('org.illumium', function(err, service){
  if(err){
    throw err;
  }

  service.node('/', function(err, node){
    if(err){
      throw err;
    }

    node.iface('org.illumium.TestService', {
      method: {
        TestMethod: {
          arg: {
            testString: 's',
            testNumber: 'd',
            testStruct: '(is)',
            testArray:  'ad',
            testObject: 'a{si}'
          },
          res: {
            testString: 's',
            testNumber: 'd',
            testStruct: '(is)',
            testArray:  'ad',
            testObject: 'a{si}'
          }
        },
        SyncMethod: {
          res: {
            testString: 's',
            testComplex: 'a(ia{sv})'
          }
        }
      },
      signal: {
        TestSignal: {
          arg: {
            a: 'ai'
          }
        }
      }
    }, function(err, iface){
      if(err){
        throw err;
      }

      iface.TestMethod(function(testString, testNumber, testStruct, testArray, testObject, result){
        console.log('  method called');
        setTimeout(function(){ /* fake timeout */
          result(testString, testNumber, testStruct, testArray, testObject);
        }, 100);
      });

      iface.SyncMethod(function(){
        console.log('  sync method called');
        return ['it works too', [
          [4, {a: true, b: 'hello'}]
        ]];
      });

      DBus().proxy('org.illumium', '/', 'org.illumium.TestService', function(err, proxy){
        if(err){
          throw err;
        }

        console.log('test method call');
        proxy.TestMethod('it works', 19.1, [3, "a"], [1, 2, 3], {a: 1, b: 2},
          function(err, testString, testNumber, testStruct, testArray, testObject){
            if(err){
              throw err;
            }

            assert.strictEqual(testString, 'it works');
            assert.strictEqual(testNumber, 19.1);
            assert.deepEqual(testStruct, [3, "a"]);
            assert.deepEqual(testArray, [1, 2, 3]);
            assert.deepEqual(testObject, {a: 1, b: 2});

            console.log('  method return received ok');
          });

        console.log('test sync method call');
        proxy.SyncMethod(function(err, testString, testComplex){
          if(err){
            throw err;
          }

          assert.strictEqual(testString, 'it works too');
          assert.deepEqual(testComplex, [
            [4, {a: true, b: 'hello'}]
          ]);

          console.log('  sync method return received ok');
        });

        proxy.TestSignal.on(function(a){
          assert.deepEqual(a, [1, 2, 3]);
          console.log('  signal received ok');
        });

        console.log('test signal emit');
        iface.TestSignal([1, 2, 3]);
      });
    });
  });
});
