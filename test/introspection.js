var FS = require('fs'),
    Introspection = require('../lib/introspection'),
    assert = require('assert'),

    tests = [
      'simple',
      'org.bluez',
      'org.ofono',
      'org.ofono.modem'
    ];

for(var i = 0; i < tests.length; i++){
  test(tests[i]);
}

function per_line_diff(actual, expect){
  actual = actual.split('\n');
  expect = expect.split('\n');
  var diff = [],
      i = 0,
      count = Math.max(actual.length, expect.length);
  for(; i < count; i++){
    if(actual[i] !== expect[i]){
      diff.push(i + ':' + '\n  actual: ' + actual[i] + '\n  expect: ' + expect[i]);
    }
  }
  return diff.join('\n');
}

function test(name){
  var source_xml = FS.readFileSync(__dirname + '/introspection/' + name + '.xml', 'utf8'),
      source_jso = JSON.parse(FS.readFileSync(__dirname + '/introspection/' + name + '.spec', 'utf8')),
      parsed_jso,
      rendered_xml;

  console.log('test', name);

  parsed_jso = Introspection.parse(source_xml);

  var parsed_str = JSON.stringify(parsed_jso, null, '  '),
      source_str = JSON.stringify(source_jso, null, '  ');

  try{
    assert.deepEqual(parsed_jso, source_jso);
    console.log('  parsing is ok');
  }catch(e){
    console.error('Parsed introspection from ' + name + '.xml is unexpected\n', e.message, per_line_diff(parsed_str, source_str));
  }

  rendered_xml = Introspection.format(source_jso, '  ');

  try{
    assert.strictEqual(rendered_xml, source_xml);
    console.log('  formatting is ok');
  }catch(e){
    console.error('Rendered introspection from ' + name + '.json is unexpected\n', e.message, per_line_diff(rendered_xml, source_xml));
  }
}
